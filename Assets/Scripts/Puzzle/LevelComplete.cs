using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelComplete : Singleton<LevelComplete>
{
    private int pointsToWin;
    private int currentPoints;
    //public GameObject myGun;
    //public GameObject Level2;

    public void Init(Transform myGun)
    {
        pointsToWin = myGun.childCount;
        //pointsToWin = Level2.transform.childCount;
    }

    void Update()
    {
        if (currentPoints >= pointsToWin)
        {
            //WIN
            //transform.GetChild(0).gameObject.SetActive(true);
            Won();
        }
    }
    public void AddPoints()
    {
        currentPoints++; 
    }
    void Won()
    {
        A.LevelCompleted();
    }
}
