using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSystem : MonoBehaviour
{
    public GameObject correctForm;
    private bool moving;
    private bool finish;

    private float startPosX;
    private float startPosY;

    private float startRotX;
    private float startRotY;

    private Rigidbody2D rb;

    public static bool GameOver = false;


    private Quaternion resetRotation;

    private Vector3 resetPosition;
    void Start()
    {
        resetPosition = this.transform.localPosition;
        resetRotation = this.transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        if(finish == false)
        {
            if (moving)
            {
                Vector3 mousePos;
                mousePos = Input.mousePosition;
                mousePos = Camera.main.ScreenToWorldPoint(mousePos);

                this.gameObject.transform.localPosition = new Vector3(mousePos.x - startPosX, mousePos.y - startPosY, this.gameObject.transform.localPosition.z);
            }
        }
        

    }
    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            startPosX = mousePos.x - this.transform.localPosition.x;
            startPosY = mousePos.y - this.transform.localPosition.y;

            startRotX = mousePos.x - this.transform.localRotation.x;
            startRotY = mousePos.y - this.transform.localRotation.y;


            moving = true;
        }
    }
    private void OnMouseUp()
    {
        moving = false;

        if (Mathf.Abs(this.transform.localPosition.x - correctForm.transform.localPosition.x) <= 0.5f &&
            Mathf.Abs(this.transform.localPosition.y - correctForm.transform.localPosition.y) <= 0.5f &&
            Mathf.Abs(this.transform.localRotation.x - correctForm.transform.localRotation.x) <= 0.5f &&
            Mathf.Abs(this.transform.localRotation.y - correctForm.transform.localRotation.y) <= 0.5f)
        {
            this.transform.position = new Vector3(correctForm.transform.position.x, correctForm.transform.position.y, correctForm.transform.position.z);
            this.transform.rotation = new Quaternion(0,correctForm.transform.rotation.x, correctForm.transform.rotation.y, correctForm.transform.rotation.z);

            finish = true;
            
            GameObject.Find("PointHandler").GetComponent<LevelComplete>().AddPoints();
            
        }
        else
        {
            this.transform.localPosition = new Vector3(resetPosition.x, resetPosition.y, resetPosition.z);
            
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("PuzzlePart"))
        {
            A.GC.GameOver();
            SetPuzzlePiecesKinematic(true);
            Debug.Log("GAME OVER");
        }
        
    }
    private void SetPuzzlePiecesKinematic(bool isKinematic)
    {
        GameObject[] puzzlePieces = GameObject.FindGameObjectsWithTag("PuzzlePart");

        foreach (GameObject piece in puzzlePieces)
        {
            Rigidbody2D rb = piece.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.isKinematic = isKinematic;
            }
        }
    }

}
