﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSpawner : Singleton<LevelSpawner> {
    //public List<Level> levels;
    private int currentLevelIndex = 0;

    public GameObject[] levels;
    
    public void Init() {
        if (Data.Level.I() > 4)
        {
            LoadLevel();
        }
        else
        {
            levels = Resources.LoadAll<GameObject>("Levels");
            GameObject temp = Instantiate(levels[Data.Level.I() - 1], Vector3.zero, Q.Euler(Vector3.zero), transform);
            LevelComplete.Instance.Init(temp.Child(0));
        }
        
        
    }
    public void LoadLevel()
    {
        currentLevelIndex++;
        if (currentLevelIndex >= levels.Length)
        {
            // All levels completed, display game completion message or perform other actions
            Debug.Log("Congratulations! You completed all levels!");
            return;
        }

        Instantiate(levels[currentLevelIndex], Vector3.zero, Quaternion.Euler(Vector3.zero), transform);
    }
    // Example usage to switch to the next level
    public void SwitchToNextLevel()
    {
        LoadLevel();
    }



}